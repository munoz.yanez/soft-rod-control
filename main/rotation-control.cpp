
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>

#include "Device.h"
#include "fcontrol.h"
#include "IPlot.h"

#include "sensors/NiclaVision.h"
#include "actuators/SWave.h"


using namespace std;

int main()
{

    //Process parameters
    double dts=0.050; //50 ms
    double tf=10;
    int N=int(tf/dts); //10 seconds
    SamplingTime Ts(dts);


    //Servo command
    SerialComm a1comm("/dev/ttyUSB0",115200);
    SWave a1(a1comm);

    //Sensor fusion in Nicla vision
    SerialComm s1comm("/dev/ttyACM0",115200);
    NiclaVision s1(s1comm,dts);


    //Variables
//        vector<double> tgt0={0.00, 0.06, 0.17, -1.2, 0. };
    //    vector<double> tgt0={0.08, -0.02, 0.16, +0.0, 1.25};
        vector<double> tgt0={0.0, 0.0, 0.18, +0.0, 0.0};
    vector<double> tgt=tgt0;
    vector< vector<double> > sensor(4);
    a1.SetThrottle(tgt);

//    for (int i=0; i<100; i++)
//    {
//        s1.GetAll();
//    }
//    cout << "IMU OK." << endl;
    sleep(3);

    //Plots
    IPlot pout(dts, "Pitch output", "t(s)", "Pitch (rad)");
    IPlot pin(dts, "Pitch input", "t(s)", "Target (rad)");

    IPlot rin(dts, "Roll input", "t(s)", "Target (rad)");
    IPlot rout(dts, "Roll output", "t(s)", "Roll (rad)");
    IPlot pre(dts, "Roll error", "t(s)", "Error (rad)");


    //sinusoidal specifications
    double w=1.0,A=1.0;

    double t=0;
    double rt=tgt0[3]; //roll target
    double re=0; //roll error
    double rs=0; //roll sensor

    double pt=tgt0[4]; //pitch target
    double pe=0; //pitch error
    double ps=0; //pitch sensor


    double ys=0; //y sensor

    PID rc(0.15,0.99,0,dts);
    PID pc(0.15,0.99,0,dts);

//    FPD rc(0.5199,1.053,-1.23,dts);

    //filters
    double f1Tw=10*dts; //filter 1 cutoff frequency
//    TF rf10(0.2,0.2,- 0.6,1); //10 rad
    TF rf10(0.33,0.33,- 0.33,1); //10 rad

//    TF pf10(0.2,0.2,- 0.6,1); //10 rad
    TF pf10(0.33,0.33,- 0.33,1); //10 rad
    TF yf10(0.33,0.33,- 0.33,1); //10 rad

//    TF f1(0.04762,0.04762,- 0.9048,1); //2 rad
//    TF f1(0.02439,0.02439,- 0.9512,1); //1 rad

    rf10.Reset(tgt[3]);
    pf10.Reset(tgt[4]);

    int mv=1;
    double dist=0.01; //meters (0.01=1cm) or radians (0.7=45 deg)

    for (int i=0; i<N; i++)
    {

        t=i*dts;
        tgt[0]=tgt0[0]+1*dist*(t/tf);
//        tgt[1]=tgt0[1]+dist*(t/tf);

        sensor = s1.GetAll();
//        s1.PrintAll();
        rs = -sensor[3][0] > rf10;
        ps = sensor[3][1] > pf10;
        ys = -sensor[1][1] > yf10; //0.01 -> measure seems cemtimeters and loop units are meters

        re=rt-rs;
        pe=pt-ps;
//        pre.pushBack(sensor[0][1]);
//        pout.pushBack(sensor[0][2]);

//        cout << "S0: " << sensor[1][0] << ", S1: " << sensor[1][1] << endl;
        cout << "Pos: " << ys <<  endl;

        tgt[3]=tgt0[3]+(re>rc);
        tgt[4]=tgt0[4]+(pe>pc);

        rout.pushBack(ys);
        pout.pushBack(ps);

        a1.SetThrottle(tgt);

        Ts.WaitSamplingTime();
    }


//    pre.Plot();
    rout.Plot();
    pout.Plot();

    //    pout.PlotAndSave("pout.A"+to_string(A)+"w"+to_string(w)+".csv");
    //    pin.PlotAndSave("pin.A"+to_string(A)+"w"+to_string(w)+".csv");


    a1.SetThrottle(tgt0);

    sleep(3);

    return 0;
}





