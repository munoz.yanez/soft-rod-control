
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>

#include "Device.h"
#include "fcontrol.h"
#include "IPlot.h"

#include "sensors/NiclaVision.h"
#include "actuators/SWave.h"

using namespace std;

int main()
{

    //Process parameters
    double dts=0.050; //50 ms
    double tf=10;
    int N=int(tf/dts); //10 seconds
    SamplingTime Ts(dts);


    //Sensor fusion in Nicla vision
    SerialComm s1comm("/dev/ttyACM0",115200);
    NiclaVision s1(s1comm,dts);
    //    s1.get("t");
    //    s1.get(to_string(dts));


    //Variables

    //Plots
    IPlot pl1(dts, "readings[0][0]", "t(s)", "");
    IPlot pl2(dts, "Pitch input", "t(s)", "Target (rad)");
    IPlot pl3(dts, "readings[0][1]", "t(s)", "");
    IPlot pl4(dts, "Roll input", "t(s)", "Target (rad)");
    vector< vector<double> > readings(3);


    double t=0;
    double in=0;


    //Servo command
    SerialComm a1comm("/dev/ttyUSB0",115200);
    SWave a1(a1comm);


    //Variables
    //    vector<double> tgt={0, 0, 0, 0.0, 0};
//    vector<double> tgt0={0.00, 0.00, 0.18, 0.0, 0 };
    vector<double> tgt0={0.07, 0.00, 0.16, 1.3, 0.0};
    vector<double> tgt=tgt0;

    a1.SetThrottle(tgt0);
    sleep(3);

//    tgt[0]+=0.4;
    a1.SetThrottle(tgt);
    int mv=2;
    double dist=+0.01; //meters


    for (int i=0; i<N; i++)
    {
        t=i*dts;
//        tgt[1]=-t/10;
        tgt[mv]=tgt0[mv]+dist*(t/tf);

        readings = s1.GetAll();
        pl1.pushBack(readings[3][0]);
        pl3.pushBack(readings[3][1]);
        a1.SetThrottle(tgt);

//        cout << readings[1][2] << endl;

        Ts.WaitSamplingTime();
    }


    pl1.Plot();
    pl3.Plot();

    //    pin.Plot();
    a1.SetThrottle(tgt0);
    sleep(3);

    return 0;
}





