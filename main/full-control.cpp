
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>

#include "Device.h"
#include "fcontrol.h"
#include "IPlot.h"

#include "sensors/NiclaVision.h"
#include "actuators/SWave.h"


using namespace std;

int main()
{

    //Process parameters
    double dts=0.050; //50 ms
    double tf=10;
    int N=int(tf/dts); //10 seconds
    SamplingTime Ts(dts);


    //Servo command
    SerialComm a1comm("/dev/ttyUSB1",115200);
    SWave a1(a1comm);

    //Sensor fusion in Nicla vision
    SerialComm s1comm("/dev/ttyACM0",115200);
    NiclaVision s1(s1comm,dts);


    //Variables
//        vector<double> tgt0={0.00, 0.06, 0.17, -1.2, 0. };
//    vector<double> tgt0={0.08, -0.0, 0.15, +0.0, 1.3};
    vector<double> tgt0={0.00, 0.00, 0.18, 0.0, 0.0};

    vector<double> tgt=tgt0;
    vector< vector<double> > sensor(4);
    a1.SetThrottle(tgt);

//    for (int i=0; i<100; i++)
//    {
//        s1.GetAll();
//    }
//    cout << "IMU OK." << endl;
    sleep(3);

    //Plots
    IPlot pout(dts, "Pitch output", "t(s)", "Pitch (rad)");
    IPlot py(dts, "y output", "t(s)", "y (??)");

    IPlot px(dts, "x output", "t(s)", "x (??)");
    IPlot rout(dts, "Roll output", "t(s)", "Roll (rad)");
    IPlot pre(dts, "Roll error", "t(s)", "Error (rad)");


    //sinusoidal specifications
    double w=1.0,A=1.0;

    double t=0;
    double rt=tgt[3]; //roll target
    double re=0; //roll error
    double rs=0; //roll sensor

    double pt=tgt[4]; //pitch target
    double pe=0; //pitch error
    double ps=0; //pitch sensor

    double yt=-0.0;//tgt[1]; //y target!!!!

    double ye=0; //y error
    double ys=0; //y sensor

    double xt=0.0;//tgt[0]; //x target!!!!!

    double xe=0; //x error
    double xs=0; //x sensor

    string filename="full_x"+to_string(xt)+"y"+to_string(yt)+".csv";
    fstream datafile;
    datafile.open (filename, fstream::out);

    datafile << "t , re, pe , xe , ye , tgt[3] , tgt[4] , tgt[0] , tgt[1] , rs , ps , xs , ys " << endl;


//    PID rc(0.15,0.99,0,dts); //w=1, pm=60
//    PID pc(0.6319,2.525,0,dts); //w=1, pm=60
//    PID yc(0.04,0.9424,0,dts); //w=1, pm=90


    PID rc(0.15,0.99,0,dts); //w=1, pm=60
    PID pc(0.15,0.99,0,dts); //w=1, pm=60
    PID yc(0.04/2,0.9424/2,0,dts); //w=1, pm=90
    PID xc(0.04/2,0.9424/2,0,dts); //w=1, pm=90

//    FPD rc(0.5199,1.053,-1.23,dts);

    //filters
    double f1Tw=10*dts; //filter 1 cutoff frequency
//    TF rf10(0.2,0.2,- 0.6,1); //10 rad
    TF rf10(0.33,0.33,- 0.33,1); //10 rad

//    TF pf10(0.2,0.2,- 0.6,1); //10 rad
    TF pf10(0.33,0.33,- 0.33,1); //10 rad
    TF yf10(0.33,0.33,- 0.33,1); //10 rad
    TF xf10(0.33,0.33,- 0.33,1); //10 rad

//    TF f1(0.04762,0.04762,- 0.9048,1); //2 rad
//    TF f1(0.02439,0.02439,- 0.9512,1); //1 rad

    rf10.Reset(tgt0[3]);
    pf10.Reset(tgt0[4]);

    for (int i=0; i<N; i++)
    {

        t=i*dts;
//        tgt[1]=tgt0[1]+dist*(t/tf);
//        tgt[2]=tgt0[2]+dist*(t/tf);

        sensor = s1.GetAll();
//        s1.PrintAll();
        rs = -sensor[3][0] > rf10;
        ps = sensor[3][1] > pf10;
        //fix the sensors to have known and accurate measurements
        ys = -0.01*sensor[1][0] > yf10; //0.01 -> measure seems cemtimeters and loop units are meters
        xs = -0.01*sensor[1][1] > xf10; //0.01 -> measure seems cemtimeters and loop units are meters


        re=rt-rs;
        pe=pt-ps;
        ye=yt-ys;
        xe=xt-xs;


//        cout << "pos: y= " << ys << ", z= " << zs << ", x?=" << sensor[0][2]  << endl;

        tgt[3]=tgt0[3]+(re>rc);
        tgt[4]=tgt0[4]+(pe>pc);
        tgt[0]=tgt0[0]+(xe>xc);
        tgt[1]=tgt0[1]+(ye>yc);

//        tgt[1]=tgt0[1]+1*0.02*(t/tf);

        rout.pushBack(rs);
        pout.pushBack(ps);
        py.pushBack(ys);
        px.pushBack(xs);

        datafile << t << " , "
                 << re << " , " << pe << " , " << xe << " , " << ye << " , "
                 << tgt[3] << " , " << tgt[4] << " , " << tgt[0] << " , " << tgt[1] << " , "
                 << rs << " , " << ps << " , " << xs << " , " << ys << " , "
                 << endl;


        a1.SetThrottle(tgt);

        Ts.WaitSamplingTime();
    }


//    py.PlotAndSave("xpos.csv");
    py.Plot();
    px.Plot();

    rout.Plot();
    pout.Plot();

    //    pout.PlotAndSave("pout.A"+to_string(A)+"w"+to_string(w)+".csv");
    //    pin.PlotAndSave("pin.A"+to_string(A)+"w"+to_string(w)+".csv");


    a1.SetThrottle(tgt0);

    datafile.close();

    sleep(3);

    return 0;
}





