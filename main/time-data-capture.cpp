
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>

#include "Device.h"
#include "fcontrol.h"
#include "IPlot.h"

#include "sensors/NiclaVision.h"
#include "actuators/SWave.h"


using namespace std;

int main()
{

    //Process parameters
    double dts=0.050; //50 ms
    int N=int(3/dts); //10 seconds
    SamplingTime Ts(dts);


    //Servo command
    SerialComm a1comm("/dev/ttyUSB0",115200);
    SWave a1(a1comm);

    //Sensor fusion in Nicla vision
    SerialComm s1comm("/dev/ttyACM0",115200);
    NiclaVision s1(s1comm,dts);


    //Variables
    //        vector<double> tgt0={0.00, 0.00, 0.18, 0., 0. };
//    vector<double> tgt0={0.06, 0.00, 0.17, -0.6, 1.0}; //towards x act 3
    vector<double> tgt0={0.00, -0.07, 0.17, 1.0, -0.6}; //towards y act 4

    vector<double> tgt=tgt0;
    vector< vector<double> > sensor(4);
    a1.SetThrottle(tgt);
    sleep(3);
    //Plots
    IPlot pout(dts, "Angle output", "t(s)", "Pitch (rad)");
    IPlot pin(dts, "Angle input", "t(s)", "Target (rad)");


    //step specifications
    double A=0.25;
    int act=4;
    vector<double> rot={0,0,0}; //intrinsic ee euler angles XYZ
    int reps=2;

    double t=0;
    double in=0;

    string filename="io_steps"+to_string(A)+"DOF"+to_string(act)+".csv";
    fstream datafile;
    datafile.open (filename, fstream::out);

    datafile << " Initial target pos: "
             << tgt0[0] << " , " << tgt0[1] << " , " << tgt0[2] << endl
             << " Initial target rot: "
             << tgt0[3] << " , " << tgt0[4] << " , " << tgt0[5] << endl ;

    //filters
    double f1Tw=10*dts; //filter 1 cutoff frequency
    TF f1(0.02439,0.02439,- 0.9512,1);
    f1.Reset(tgt[3]);

    //online identification
    RLS model(0,1);


    for (int rep=-reps; rep<=reps; rep++)
    {
        for (int i=0; i<N; i++)
        {
            t=i*dts;
            sensor = s1.GetAll();
            rot=sensor[0];

            //target
            in=A*rep;
            pin.pushBack(in);


            tgt[act]=in;
            a1.SetThrottle(tgt);

            datafile << t << " , " << in << " , "
                     << rot[0] << " , " << rot[1] << " , " << rot[2] << ""
                     << endl;


            model.UpdateSystem(in,rot[act-3]);
            pout.pushBack(rot[act-3]);

            Ts.WaitSamplingTime();
        }
    }


    pout.Plot();
    pin.Plot();
    //    pout.PlotAndSave("pout.A"+to_string(A)+"w"+to_string(w)+".csv");
    //    pin.PlotAndSave("pin.A"+to_string(A)+"w"+to_string(w)+".csv");

    cout << "Saved at: " << filename << endl;
    datafile.close();

    a1.SetThrottle(tgt0);

    sleep(2);
    model.PrintZTransferFunction(dts);

    return 0;
}





