
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>

#include "Device.h"
#include "fcontrol.h"
#include "IPlot.h"

#include "sensors/NiclaVision.h"
#include "actuators/SWave.h"


using namespace std;

int main()
{

    //Process parameters
    double dts=0.100; //10 ms
    int N=int(10/dts); //10 seconds
    SamplingTime Ts(dts);


    //Servo command
    SerialComm a1comm("/dev/ttyUSB0",115200);
    SWave a1(a1comm);

    //Sensor fusion in Nicla vision
    SerialComm s1comm("/dev/ttyACM0",115200);
    NiclaVision s1(s1comm,dts);


    //Variables
    vector<double> tgt={0.0, 0.0, 0.0, 0.0, 0.0 };
    vector<double> sensor{0};

    //Plots
    IPlot pout(dts, "Pitch output", "t(s)", "Pitch (rad)");
    IPlot pin(dts, "Pitch input", "t(s)", "Target (rad)");


    //sinusoidal specifications
    double w=1.0,A=1.0;

    double t=0;
    double in=0;




    for (int i=0; i<N; i++)
    {
        t=i*dts;
        sensor = s1.get("p");

        pout.pushBack(sensor[0]);

        //target
        in=A*sin(w*t);
        pin.pushBack(in);


        tgt[3]=in;
        a1.SetThrottle(tgt);


        Ts.WaitSamplingTime();
    }


    pout.Plot();
    pin.Plot();
//    pout.PlotAndSave("pout.A"+to_string(A)+"w"+to_string(w)+".csv");
//    pin.PlotAndSave("pin.A"+to_string(A)+"w"+to_string(w)+".csv");


    a1.SetThrottle(vector<double>{0,0});

    sleep(2);

    return 0;
}





