
#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>

#include "Device.h"
#include "fcontrol.h"
#include "IPlot.h"

#include "sensors/NiclaVision.h"
#include "actuators/SWave.h"


using namespace std;

int main()
{

    //Process parameters
    double dts=0.050; //50 ms
    double tf=10;
    int N=int(tf/dts); //10 seconds
    SamplingTime Ts(dts);


    //Sensor fusion in Nicla vision
    SerialComm s1comm("/dev/ttyACM0",115200);
    NiclaVision s1(s1comm,dts);
//    s1.get("t");
//    s1.get(to_string(dts));


    //Variables
    vector<double> tgt={0.0, 0.0};
    vector<double> sensor{0,0};

    //Plots
    IPlot pout(dts, "Pitch output", "t(s)", "Pitch (rad)");
    IPlot pin(dts, "Pitch input", "t(s)", "Target (rad)");


    //sinusoidal specifications
    double w=10.0,A=1.5;

    double t=0;
    double in=0;

    vector< vector<double> > readings(3);


    for (int i=0; i<N; i++)
    {
        t=i*dts;
//        sensor = s1.get("d");
        //        pout.pushBack(sensor[0]);
//        cout << sensor[0] << ", " << sensor[1] << endl ;

//        cout << s1.GetLine() << endl ;

        s1.PrintAll();
//        cout << readings[0][0] << ", " << readings[0][1] << endl ;


        Ts.WaitSamplingTime();
    }


//    pout.Plot();
//    pin.Plot();

    sleep(2);

    return 0;
}





