#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

void f1() {
   for (int i = 0; i < 5; ++i) {
      std::cout << "1st Thread executing\n";
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
   }
}



int main() {
   int n = 0;

   std::thread t2(f1);

   t2.join();
   std::cout << "Final value of n is " << n << '\n';
}
